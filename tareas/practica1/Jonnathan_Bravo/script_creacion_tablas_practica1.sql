-- Creacion de tabla INFO_CLIENTES --
create table INFO_CLIENTES
(
  id_cliente NUMBER not null,
  nombre     VARCHAR2(250) not null,
  direccion  VARCHAR2(500) not null,
  telefono   VARCHAR2(15) not null
);
-- Add comments to the columns 
comment on column INFO_CLIENTES.id_cliente
  is 'Id del cliente';
comment on column INFO_CLIENTES.nombre
  is 'Nombre del cliente';
comment on column INFO_CLIENTES.direccion
  is 'Direccion del cliente';
comment on column INFO_CLIENTES.telefono
  is 'Telefono del cliente';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CLIENTES
  add constraint PK_INFO_CLIENTES primary key (ID_CLIENTE);

  
  
-- Creacion de tabla INFO_FACTURAS --
create table INFO_FACTURAS
(
  id_factura NUMBER not null,
  fecha      DATE not null,
  cliente_id NUMBER not null
);
-- Add comments to the columns 
comment on column INFO_FACTURAS.id_factura
  is 'Id de la factura';
comment on column INFO_FACTURAS.fecha
  is 'Fecha de la factura';
comment on column INFO_FACTURAS.cliente_id
  is 'id de referencia al cliente';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_FACTURAS
  add constraint PK_INFO_FACTURAS primary key (ID_FACTURA);
  
alter table INFO_FACTURAS
  add constraint FK_INFO_FACTURAS_CLIENTES foreign key (CLIENTE_ID)
  references INFO_CLIENTES (ID_CLIENTE);
  
  
  
-- Creacion de tabla INFO_CATEGORIAS
create table INFO_CATEGORIAS
(
  id_categoria NUMBER not null,
  descripcion  VARCHAR2(250) not null
);
-- Add comments to the columns 
comment on column INFO_CATEGORIAS.id_categoria
  is 'Id de la categoria de producto';
comment on column INFO_CATEGORIAS.descripcion
  is 'Descripcion de la categoria de producto';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_CATEGORIAS
  add constraint PK_INFO_CATEGORIAS primary key (ID_CATEGORIA);
  
  
  
-- Creacion de la tabla INFO_PROVEEDORES
create table INFO_PROVEEDORES
(
  id_proveedor NUMBER not null,
  nombre       VARCHAR2(250) not null,
  direccion    VARCHAR2(1000) not null,
  telefono     VARCHAR2(15) not null
);
-- Add comments to the columns 
comment on column INFO_PROVEEDORES.id_proveedor
  is 'Id del proveedor';
comment on column INFO_PROVEEDORES.nombre
  is 'Nombre del proveedor';
comment on column INFO_PROVEEDORES.direccion
  is 'Direccion del proveedor';
comment on column INFO_PROVEEDORES.telefono
  is 'Telefono del proveedor';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PROVEEDORES
  add constraint PK_INFO_PROVEEDORES primary key (ID_PROVEEDOR);
  
  
-- Creacion de la tabla INFO_PRODUCTOS
create table INFO_PRODUCTOS
(
  id_producto  NUMBER not null,
  descripcion  VARCHAR2(1000) not null,
  precio       NUMBER default 0.0 not null,
  categoria_id NUMBER not null,
  proveedor_id NUMBER not null
);
-- Add comments to the columns 
comment on column INFO_PRODUCTOS.id_producto
  is 'Id del producto';
comment on column INFO_PRODUCTOS.descripcion
  is 'Descripcion del producto';
comment on column INFO_PRODUCTOS.precio
  is 'Precio del producto';
comment on column INFO_PRODUCTOS.categoria_id
  is 'Id de referencia a la categoria del producto';
comment on column INFO_PRODUCTOS.proveedor_id
  is 'Id de referencia al proveedor';
  
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_PRODUCTOS
  add constraint PK_INFO_PRODUCTOS primary key (ID_PRODUCTO);
alter table INFO_PRODUCTOS
  add constraint FK_INFO_PRODUCTOS_CATEGORIA foreign key (CATEGORIA_ID)
  references INFO_CATEGORIAS (ID_CATEGORIA);
alter table INFO_PRODUCTOS
  add constraint FK_INFO_PRODUCTOS_PROVEEDORES foreign key (PROVEEDOR_ID)
  references INFO_PROVEEDORES (ID_PROVEEDOR);
  
  
-- Creacion de tabla INFO_VENTAS
create table INFO_VENTAS
(
  id_venta    NUMBER not null,
  cantidad    NUMBER default 0 not null,
  factura_id  NUMBER not null,
  producto_id NUMBER not null
);
-- Add comments to the columns 
comment on column INFO_VENTAS.id_venta
  is 'Id de la venta';
comment on column INFO_VENTAS.cantidad
  is 'Cantidad de productos vendidos';
comment on column INFO_VENTAS.factura_id
  is 'Id de referencia a la factura';
comment on column INFO_VENTAS.producto_id
  is 'Id de referencia al producto';
-- Create/Recreate primary, unique and foreign key constraints 
alter table INFO_VENTAS
  add constraint PK_INFO_VENTAS primary key (ID_VENTA);
alter table INFO_VENTAS
  add constraint FK_INFO_VENTAS_FACTURAS foreign key (FACTURA_ID)
  references INFO_VENTAS (ID_VENTA);
alter table INFO_VENTAS
  add constraint FK_INFO_VENTAS_PRODUCTO foreign key (PRODUCTO_ID)
  references INFO_PRODUCTOS (ID_PRODUCTO);
  

  
  